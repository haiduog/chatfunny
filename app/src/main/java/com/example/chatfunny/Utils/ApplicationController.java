package com.example.chatfunny.Utils;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class ApplicationController extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }
}
