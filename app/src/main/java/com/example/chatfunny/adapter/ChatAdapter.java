package com.example.chatfunny.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatfunny.R;
import com.example.chatfunny.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private String myId;
    private List<ChatMessage> messageList= new ArrayList<>();
    private final int ID_SEND = 0;
    private final int ID_REV = 1;
    public ChatAdapter(Context context ,String myId, List<ChatMessage> messageList) {
        this.myId = myId;
        this.messageList = messageList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == ID_SEND){
            view = LayoutInflater.from(context).inflate(R.layout.item_chat_send, parent, false);
            return new SendTextViewHolder(view);
        }else {
            view = LayoutInflater.from(context).inflate(R.layout.item_chat_reiceve, parent, false);
            return new ReceiveTextViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ID_SEND)
          ((SendTextViewHolder)holder).tvChat.setText(messageList.get(position).getMessageText());
        else {
            ((ReceiveTextViewHolder)holder).tvChat.setText(messageList.get(position).getMessageText());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (myId.equals(messageList.get(position).getIdSender()))
            return ID_SEND;
        return ID_REV;
    }

    @Override
    public int getItemCount() {
        if (messageList == null)
            return 0;
        return messageList.size();
    }

    public class SendTextViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_chat)
        EmojiTextView tvChat;
        public SendTextViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ReceiveTextViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_chat)
        EmojiTextView tvChat;
        public ReceiveTextViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface IOnClick{
        void onClick();
    }
}
