package com.example.chatfunny.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatfunny.MainActivity;
import com.example.chatfunny.R;
import com.example.chatfunny.activity.DetailChatActivity;
import com.example.chatfunny.model.Account;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<Account> listUser = new ArrayList<>();
    private Context context;
    IOnClick iOnClick;

    public UserAdapter(List<Account> listUser, Context context, IOnClick iOnClick) {
        this.listUser = listUser;
        this.context = context;
        this.iOnClick = iOnClick;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, int position) {
        Account model = listUser.get(position);
        Glide.with(context).load(model.getImage()).into(holder.imgAvatar);
        holder.messageUser.setText(model.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnClick.onClick(model);
            }
        });


    }

    @Override
    public int getItemCount() {
        return listUser == null ? 0 : listUser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        TextView messageUser;
        ShapeableImageView imgAvatar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
             messageText = itemView.findViewById(R.id.message_text);
             messageUser = itemView.findViewById(R.id.message_user);
             imgAvatar = itemView.findViewById(R.id.img_avatar);
        }
    }

    public interface IOnClick{
        void onClick(Account model);
    }
}
