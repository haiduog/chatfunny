package com.example.chatfunny.model;

public class Account {
    private String uid;
    private String name;
    private String image;
    private String email;

    public Account(String uid, String name, String image, String email) {
        this.uid = uid;
        this.name = name;
        this.image = image;
        this.email = email;
    }

    public Account() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
