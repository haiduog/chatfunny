package com.example.chatfunny.model;

import java.util.List;

public class Post {
    private String id;
    private List<ChatMessage> list = null;

    public Post(String id, List<ChatMessage> list) {
        this.id = id;
        this.list = list;
    }

    public Post() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ChatMessage> getList() {
        return list;
    }

    public void setList(List<ChatMessage> list) {
        this.list = list;
    }
}
