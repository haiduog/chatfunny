package com.example.chatfunny.model;

import java.util.Date;
import java.util.List;

public class ChatMessage {
    private String idSender;
    private String messageText;
    private long messageTime;

    public ChatMessage(String idSender, String messageText) {
        this.idSender = idSender;
        this.messageText = messageText;
        messageTime = new Date().getTime();
    }

    public ChatMessage() {
    }

    public String getIdSender() {
        return idSender;
    }

    public void setIdSender(String idSender) {
        this.idSender = idSender;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }
}
