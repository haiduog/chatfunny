package com.example.chatfunny.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.bundled.BundledEmojiCompatConfig;
import androidx.emoji.text.EmojiCompat;
import androidx.emoji.widget.EmojiEditText;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatfunny.R;
import com.example.chatfunny.adapter.ChatAdapter;
import com.example.chatfunny.model.ChatMessage;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailChatActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.rcv_message)
    RecyclerView rcvMessage;
    @BindView(R.id.btn_send)
    ImageView btnSend;
    @BindView(R.id.edt_message)
    EmojiEditText edtMessage;
    @BindView(R.id.img_avatar)
    ShapeableImageView imageAvatar;
    @BindView(R.id.btn_back)
    ImageView btnBack;
    ChatAdapter chatAdapter;
    FirebaseDatabase mFirebaseDatabase;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mDatabase;
    DatabaseReference mDatabasePost;
    String keyPost;
    String urlAvatar;
    String myID = "";
    List<ChatMessage> messageList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmojiCompat.Config config = new BundledEmojiCompatConfig(this);
        EmojiCompat.init(config);
        initLayout();
        initFireBase();
        myID = mFirebaseAuth.getCurrentUser().getUid();
        setContentView(R.layout.activity_chat_detail);
        ButterKnife.bind(this);
        Glide.with(this).load(urlAvatar).into(imageAvatar);
        btnSend.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        initAdapter();
    }

    private void initLayout() {
        keyPost = getIntent().getStringExtra("key");
        urlAvatar = getIntent().getStringExtra("image");
    }

    private void initFireBase() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = mFirebaseDatabase.getReference();
        mDatabasePost = mFirebaseDatabase.getReference("Post").child(keyPost);
    }

    private void initAdapter() {
        mDatabasePost.child("message").orderByChild("messageTime").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (messageList.size() > 0) {
                    messageList.clear();
                }
                for (DataSnapshot data : snapshot.getChildren()) {
                    ChatMessage model = data.getValue(ChatMessage.class);
                    messageList.add(model);
                }
                if (chatAdapter == null) {
                    chatAdapter = new ChatAdapter(DetailChatActivity.this, myID, messageList);
                    rcvMessage.setAdapter(chatAdapter);
                    chatAdapter.notifyDataSetChanged();
                }
                if (messageList.size() > 0)
                    chatAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void sendMessage() {
        mDatabasePost.child("message").push().setValue(new ChatMessage(myID,
                edtMessage.getText().toString()));
        edtMessage.setText("");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                sendMessage();
                break;
            case R.id.btn_back:
                finish();
        }
    }
}
