package com.example.chatfunny.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.chatfunny.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateInforActivity extends AppCompatActivity {

    private static final int GALLERY_CODE = 99;
    private static final int CAMERA_CODE = 98;
    private final String TAG = getClass().getName();
    private static StorageReference sStorageReference = FirebaseStorage.getInstance().getReference();
    private boolean isUploaded = false;
    private String mUriImage = "";
    @BindView(R.id.submit)
    Button mSubmit;
    @BindView(R.id.img_avatar)
    ShapeableImageView avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_infor);
        ButterKnife.bind(this);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, GALLERY_CODE);
            }
        });
        mSubmit.setEnabled(false);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUploaded) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(user.getDisplayName())
                            .setPhotoUri(Uri.parse(mUriImage))
                            .build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "User profile updated.");
                                        Intent intent = new Intent();
                                        intent.putExtra("urlImage", mUriImage);
                                        setResult(RESULT_OK, intent);
                                    } else {
                                        setResult(RESULT_CANCELED);
                                    }
                                    finish();
                                }
                            });
                }else {
                    Toast.makeText(UpdateInforActivity.this, "Upload ảnh lỗi vui lòng thử lại!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CODE && resultCode == RESULT_OK) {
            //Get uri
            final Uri uri = data.getData();
            avatar.setImageURI(uri);
            //Send photo to Photos folder  and set name for the photo by using uri.getLastPathSegment()
            final StorageReference filePath = sStorageReference.child("Photos").child(uri.getLastPathSegment());
            filePath.putFile(uri);
            filePath.getDownloadUrl().addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //Show toast if failure occurs
                    Toast.makeText(UpdateInforActivity.this, "Failure", Toast.LENGTH_LONG).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    mUriImage = uri.toString();
                    Log.e("link : ", "" + uri.toString());
                }
            });
        }
        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK && data != null) {
            //Get uri
            Uri selectedImage = data.getData();
            String[] filePathColum = {MediaStore.Images.Media.DATA};
            avatar.setImageURI(selectedImage);
            Cursor cursor = getContentResolver().query(selectedImage, filePathColum, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int columIndex = cursor.getColumnIndexOrThrow(filePathColum[0]);
                    String path = cursor.getString(columIndex);
                    int startPosition = path.lastIndexOf('/');
                    int length = path.length();
                    String pathCode = "";
                    //get the substring which will be the name of photo
                    for (int i = startPosition + 1; i < length; i++) {
                        pathCode += path.charAt(i);
                    }
                    final StorageReference filePath = sStorageReference.child("Photos").child(pathCode);
                    filePath.putFile(selectedImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            filePath.getDownloadUrl().addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    //Show toast if failure occurs
                                    Toast.makeText(UpdateInforActivity.this, "Failure", Toast.LENGTH_LONG).show();
                                    isUploaded = false;
                                }
                            }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    isUploaded = true;
                                    mSubmit.setEnabled(true);
                                    mUriImage = uri.toString();
                                    Log.e("link : ", "" + uri.toString());

                                }
                            });
                        }
                    });

                }
                cursor.close();
            }
        }
    }
}