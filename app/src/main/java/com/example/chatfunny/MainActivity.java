package com.example.chatfunny;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.chatfunny.Utils.Utils;
import com.example.chatfunny.activity.DetailChatActivity;
import com.example.chatfunny.activity.UpdateInforActivity;
import com.example.chatfunny.adapter.UserAdapter;
import com.example.chatfunny.model.Account;
import com.example.chatfunny.model.ChatMessage;
import com.example.chatfunny.model.Post;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements UserAdapter.IOnClick {

    private static final int SIGN_IN_REQUEST_CODE = 100;
    private static final int UPDATE_REQUEST_CODE = 200;
    FirebaseListAdapter<Account> adapter;
    FirebaseDatabase mFirebaseDatabase;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseUser;
    DatabaseReference mDatabasePost;
    UserAdapter userAdapter;
    String keyPost = "";
    List<Account> listUser = new ArrayList<>();
    FirebaseUser mFirebaseUser;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLeft;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    ImageView imageAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFireBase();
        setContentView(R.layout.activity_main);
        initLayout();
        checkLoginFireBase();
        initAction();
    }

    private void initLayout(){
        ButterKnife.bind(this);
        View view = navigationView.getHeaderView(0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);
        imageAvatar = view.findViewById(R.id.img_avatar);
        if (mFirebaseUser.getPhotoUrl() != null)
            Glide.with(this).load(mFirebaseUser.getPhotoUrl()).into(imageAvatar);
    }

    private void initFireBase() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase.setPersistenceEnabled(true);
        mDatabase = mFirebaseDatabase.getReference();
        mDatabaseUser = mFirebaseDatabase.getReference("User");
        mDatabasePost = mFirebaseDatabase.getReference("Post");
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
    }

    private void initAction() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setCheckable(true);
                drawerLeft.closeDrawers();
                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLeft.openDrawer(GravityCompat.START);
            }
        });
    }

    private void displayChatMessages() {
        mDatabaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (listUser != null && listUser.size() > 0)
                    listUser.clear();
                for (DataSnapshot data : snapshot.getChildren()) {
                    if (!data.child("uid").getValue().toString().equals(mFirebaseUser.getUid())) {
                        Account model = data.getValue(Account.class);
                        listUser.add(model);
                    }
                }
                    userAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView listOfMessages = findViewById(R.id.list_of_messages);
        userAdapter = new UserAdapter( listUser, this, this::onClick);
        listOfMessages.setAdapter(userAdapter);
        listOfMessages.setLayoutManager(llm);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SIGN_IN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this,
                            "Successfully signed in. Welcome!",
                            Toast.LENGTH_LONG)
                            .show();
                    checkExistUser();
                } else {
                    Toast.makeText(this,
                            "We couldn't sign you in. Please try again later.",
                            Toast.LENGTH_LONG)
                            .show();

                    // Close the app
                    finish();
                }
                break;
            case UPDATE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    updateImage(data.getStringExtra("urlImage"));
                    Toast.makeText(this, "Update success", Toast.LENGTH_LONG)
                            .show();
                } else {
                    Toast.makeText(this, "Update err", Toast.LENGTH_LONG)
                            .show();
                }
                break;
        }


    }

    private void updateImage(String data2) {
        mDatabaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data : snapshot.getChildren()) {
                    if (data.child("uid").getValue().toString().equals(mFirebaseUser.getUid())) {
                        //do ur stuff
                        mDatabaseUser.child(data.getKey().toString()).child("image").setValue(data2);
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void checkLoginFireBase() {
        if (mFirebaseAuth.getCurrentUser() == null) {
            // Start sign in/sign up activity
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .build(),
                    SIGN_IN_REQUEST_CODE
            );
        } else {
            // User is already signed in. Therefore, display
            // a welcome Toast
            Toast.makeText(this,
                    "Welcome " + mFirebaseAuth.getCurrentUser().getDisplayName(),
                    Toast.LENGTH_LONG)
                    .show();
            checkExistUser();
            // Load chat room contents
        }
    }

    private void checkExistUser() {
        if (mDatabaseUser == null)
            mDatabaseUser = FirebaseDatabase.getInstance().getReference("User");
        if (mFirebaseUser == null)
            mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean exists = false;
                for (DataSnapshot data : snapshot.getChildren()) {
                    if (data.child("uid").getValue().toString().equals(mFirebaseUser.getUid())) {
                        //do ur stuff
                        Log.d("fb-acc", "trùng");
                        exists = true;
                    }
                }
                if (!exists) {

                    mDatabaseUser.push().setValue(new Account(mFirebaseUser.getUid(),
                            mFirebaseUser.getDisplayName(), "", mFirebaseUser.getEmail()));
                }
                displayChatMessages();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @SuppressLint("WrongConstant")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sign_out:
                AuthUI.getInstance().signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(MainActivity.this,
                                        "You have been signed out.",
                                        Toast.LENGTH_LONG)
                                        .show();

                                // Close activity
                                finish();
                            }
                        });
                break;
            case R.id.menu_update_infor:
                Intent intent = new Intent(MainActivity.this, UpdateInforActivity.class);
                startActivityForResult(intent, UPDATE_REQUEST_CODE);
                break;
        }

        return true;
    }

    @Override
    public void onClick(Account model) {
        checkExitsPost(model);
    }

    private void checkExitsPost(Account model){
        mDatabasePost.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean exists = false;
                for (DataSnapshot data : snapshot.getChildren()) {
                    String id = data.child("id").getValue().toString();
                    if (!Utils.isNull(id)){
                            if (id.contains(mFirebaseUser.getUid()) && id.contains(model.getUid())) {
                                //do ur stuff
                                Log.d("fb-acc", "trùng");
                                exists = true;
                                keyPost = data.getKey();
                                break;
                            }
                        }
                    }
                if (!exists) {
                    List<ChatMessage> list = new ArrayList<>();
                    mDatabasePost.push().setValue(new Post(mFirebaseUser.getUid() + " " + model.getUid(), list),
                            new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                    keyPost = ref.getKey();
                                }
                            });
                }
                if (keyPost.equals("")){
                    Toast.makeText(MainActivity.this,"Something Error!!!", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(MainActivity.this, DetailChatActivity.class);
                    intent.putExtra("image", model.getImage());
                    intent.putExtra("key", keyPost);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}